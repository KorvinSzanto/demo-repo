<?php

return [
    'default-connection' => 'stage',
    'connections' => [
        'stage' => [
            'driver' => 'c5_pdo_mysql',
            'server' => getenv('DB_HOSTNAME'),
            'database' => getenv('DB_DATABASE'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'charset' => getenv('DB_CHARSET'),
        ],
    ],
];
