<?php

return [
    'debug' => [
        'detail' => 'message',
        'display_errors' => false,
    ],
];
